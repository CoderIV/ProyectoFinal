/*
MATERIA: DESARROLLO DE SOFTWARE III
PROFESOR: Andy G�mez de la Torre
NOMBRE DEL PROJECTO: 
PROGRAMADORES: 
DESCRIPCI�N DEL PROGRAMA:
	Programa de interfaz gr�fica que conecta con una base de datos MySQL llamada 'examends3', y al tener
	�xito en la conexi�n, muestra una herramienta CRUD para una tabla llamada 'procesadores', la cual
	ser� generada con unos cuantos registros en caso de no estar presente en la base de datos.
*/

package MySqlCrud;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

//Clase principal del proyecto

public class Principal
{
	public static void main(String[] args)
	{
		ventana.setLayout(new GridBagLayout()); //Asignar layout a la ventana
		GridBagConstraints gridC = new GridBagConstraints();
		
		//Arreglo de componentes para la ventana
		JComponent[] contenido = {new JLabel("Nombre de usuario:"), usuario, new JLabel("Contrase�a:"), contrase�a};
		
		gridC.anchor = GridBagConstraints.NORTHWEST;
		gridC.fill = GridBagConstraints.HORIZONTAL; //Ajustar variables de posicionamiento
		gridC.insets = new Insets(16,24,8,0);
		gridC.weightx = 1;
		gridC.gridwidth = 2;
		for(int i = 0; i < 4; ++i)
		{
			gridC.gridy = i;
			ventana.add(contenido[i], gridC); //A�adir componentes del arreglo a la ventana
			gridC.insets = new Insets(0,24,4,24);
		}
		++gridC.gridy;
		gridC.insets = new Insets(16,24,4,24);
		boton.addActionListener(new ActionListener() //A�adir listener al bot�n de ingresar
		{
			@Override public void actionPerformed(ActionEvent e)
			{
				usuario.setEnabled(false);
				contrase�a.setEnabled(false); //Desactivar campos mientras se conecta a la DB
				boton.setEnabled(false);
				barra.setIndeterminate(true); //Activar animaci�n de barra de progreso
				ConnectDB conector = new ConnectDB();
				conector.usuario = usuario.getText();
				//Obtener usuario y contrase�a de los campos
				conector.contrase�a = String.valueOf(contrase�a.getPassword());
				conector.execute(); //Intentar conectar con la DB en otro hilo
			}	
		});
		ventana.add(boton, gridC);
		
		++gridC.gridy;
		gridC.weighty = 1;
		gridC.anchor = GridBagConstraints.PAGE_END;
		gridC.insets = new Insets(0,0,0,0); //Ajustar variables de posicionamiento e insertar barra de progreso
		ventana.add(barra, gridC);
		
		ventana.setSize(300, 250); //Tama�o de la ventana.
		ventana.setLocationRelativeTo(null); //Centramos la ventana
		ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE); //Asignar operaci�n en salida
		ventana.setVisible(true); //Hacer ventana visible
	}
	//Inicializamos los componentes que utilizaremos en la ventana.
	public static JFrame ventana = new JFrame("Iniciar sesi�n");
	public static JTextField usuario = new JTextField();
	public static JPasswordField contrase�a = new JPasswordField();
	public static JButton boton = new JButton("Ingresar");
	public static JProgressBar barra = new JProgressBar();
}
