package MySqlCrud;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Procesador
{
	//Creamos las variables que se utilizar�n para almacenar datos, el tipo de dato y designamos el modificador de acceso.
	private static DateTimeFormatter fmtFecha = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss"); 
	private String fabricante, modelo;
	private float frecuencia;
	private int nucleos;
	private double precio;
	private LocalDate fechasalida;

	//Constructor de la clase.
	public Procesador() {
	}

	//M�todos setter y getters para asignar y obtener datos de las variables
	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public float getFrecuencia() {
		return frecuencia;
	}

	public void setFrecuencia(float frecuencia) {
		this.frecuencia = frecuencia;
	}

	public int getNucleos() {
		return nucleos;
	}

	public void setNucleos(int nucleos) {
		this.nucleos = nucleos;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getFechasalida() {
		return fechasalida.toString();
	}

	public void setFechasalida(String fecha) {
		this.fechasalida = LocalDate.parse(fecha, fmtFecha);
	}

	//M�todo para obtener los atributos del objeto en un arreglo
	public Object[] obtenerAtrib()
	{
		return new Object[]{fabricante, modelo, frecuencia, nucleos, precio, fechasalida};
	}

}
