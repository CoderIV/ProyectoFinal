package MySqlCrud;

import java.sql.*;
import javax.swing.SwingWorker;

//Subclase de SwingWorker para conectar con la DB en otro hilo, es decir, sin afectar la ejecuci�n
//de las otras ventanas

public class ConnectDB extends SwingWorker<Void, Void>
{
	private Connection con = null; //Objeto de la conexi�n
	public String usuario = "", contrase�a = "";
	
	@Override protected Void doInBackground() throws Exception
	{
		setConnection();
		return null;
	}
	
	@Override protected void done()
	{
		if (con == null)
		{
			Principal.usuario.setEnabled(true);
			Principal.contrase�a.setEnabled(true);
			Principal.boton.setEnabled(true);
			Principal.barra.setIndeterminate(false);
			return;
		}
		Principal.ventana.dispose();
		new CRUD(this);
	}
	
	public Connection getConnection() //M�todo para devolver el objeto de la conexi�n
	{
		return con;
	}
	
	public boolean setConnection()
	{
		try
		{
			Class.forName("com.mysql.cj.jdbc.Driver"); //Cargar clase del controlador
			//Intentar establecer conexi�n con la base de datos
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/examends3", usuario, contrase�a);
		    return true;
		}
		catch (Exception e) //Si hubo un error
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage()); //Mostrar mensaje de error
			closeConnection(); //Cerrar conexi�n por si acaso
		}
		con = null;
		return false;
	}
	
	public void closeConnection() //M�todo para cerrar conexi�n con la base de datos
	{
		if (con != null) //Si la conexi�n ha sido previamente establecida
		{
			try
			{
				con.close(); //Cerrar conexi�n
			}
			catch (SQLException e) //Si hubo un error, mostrar mensaje
			{
				VentEmergentes.mostrarError("Error al cerrar conexi�n con la base de datos");
			}
		}
	}
}
