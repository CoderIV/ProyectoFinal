package MySqlCrud;

import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import javax.swing.JDialog;
import javax.swing.JFrame;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.swing.JRViewer;

@SuppressWarnings("serial")
public class Reporte extends JDialog
{
	public Reporte(JFrame ventP, Connection con)
	{
		//Llamada a superconstructor con t�tulo e indicaci�n de que debe bloquear ventana padre
		super(ventP, "Reporte", true);
		JRViewer viewer = null;
		try
        {
			InputStream reporte = getClass().getResourceAsStream("/ReporteProcesadores.jasper");
			HashMap<String, Object> param = new HashMap<String, Object>(); //Crear objeto para par�metros
			param.put("ReportTitle", "Reporte"); //Asignar par�metro ReportTitle
            JasperPrint print = JasperFillManager.fillReport(reporte, param, con);
            viewer = new JRViewer(print); //Crear vista del reporte creado
            getContentPane().add(viewer); //Agregamos la vista a la ventana.
        }
        catch (JRException j) //Manejo de excepciones
		{
        	VentEmergentes.mostrarError(j.getLocalizedMessage());
        }
        if(viewer != null) //Si la vista si se cre�
        {
        	setSize(496, 384); //Asignamos el tama�o del JDialog.
    		setLocationRelativeTo(ventP);
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setVisible(true);
        }
	}
}
