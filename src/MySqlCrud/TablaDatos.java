package MySqlCrud;

import java.awt.Dimension;
import java.sql.Connection;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

//Subclase de JTable para mostrar la tabla de la DB en el CRUD

@SuppressWarnings("serial")
public class TablaDatos extends JTable
{
	public ProcesadorModel model;
	
	TablaDatos(Connection conexion)
	{
		//Llamada a superconstructor con el nombre de las columnas
		super(new DefaultTableModel(
				 new Object[]{"Fabricante", "Modelo", "Frecuencia (GHz)", "N�cleos", "Precio", "Fecha de salida"}, 0));
		tableHeader.setReorderingAllowed(false); //Desactivar reordenamiento de columnas
		tableHeader.setPreferredSize(new Dimension(400, 36)); //Cambiar tama�o de los encabezados
		model = new ProcesadorModel(conexion); //Crear objeto del modelo de los datos en la DB
		obtenerDatos(); //Cargar datos desde la DB
	}
	
	public void restablecerTabla() //M�todo para recargar todos los registros de la DB
	{
		if(getRowCount() > 0) ((DefaultTableModel)dataModel).setRowCount(0); //Eliminar todas las filas
		obtenerDatos();
	}
	
	public void obtenerDatos() //M�todo para a�adir todos los registros de la DB a la tabla
	{
		List<Procesador> procesadores = model.buscarTodo();
		if (procesadores != null)
			for(Procesador procesador : procesadores) //A�adir cada registro como una fila nueva
				a�adirEnTabla(procesador.obtenerAtrib());
	}
	
	public void a�adirEnTabla(Object[] datos) //M�todo para a�adir un registro a la tabla como nueva fila
	{
		((DefaultTableModel)dataModel).addRow(datos);
	}
	
	public void modificarEnTabla(int fila, Object[] datos) //M�todo para cambiar los datos de una fila
	{
		for(int i = 0; i < datos.length; ++i)
			setValueAt(datos[i], fila, i);
	}
	
	public void removerSeleccion() //M�todo para remover filas seleccionadas y sus registros asociados
	{
		int fila = getSelectedRow();
		for(int i = getSelectedRowCount(); i > 0; --i)
		{
			if(model.Eliminar((String) this.getValueAt(fila, 1)) != true) break;
			((DefaultTableModel)dataModel).removeRow(fila);
		}
	}
	
	public void buscarEnTabla(String busqueda) //M�todo para buscar un registro por c�dula
	{
		((DefaultTableModel)dataModel).setRowCount(0);
		Procesador procesador = model.buscar(busqueda);
		if (procesador != null)
			a�adirEnTabla(procesador.obtenerAtrib());
	}
	
	@Override public boolean isCellEditable(int row, int col) //Desactivar edici�n de celdas
	{
		return false;
	}
}
