package MySqlCrud;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

@SuppressWarnings("serial")
public class PanelAcciones extends JPanel
{
	PanelAcciones(TablaDatos tabla)
	{
		super();
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS)); //Asignar BoxLayout horizontal como layout
		setBorder(new EmptyBorder(4,4,4,4)); //A�adir espaciado alrededor
		
		JTextField campoBuscar = new JTextField(); //Creamos el objeto del campo de texto.
		campoBuscar.setPreferredSize(new Dimension(150, 27));
		campoBuscar.setMaximumSize(new Dimension(600,27));
		add(new JLabel("Buscar:  ")); //Creamos un label.
		add(campoBuscar);
		
		BtnAcciones btnRestBusqueda = new BtnAcciones(null, tabla, campoBuscar);
		add(new BtnAcciones(btnRestBusqueda, tabla, campoBuscar)); //A�adir campo de b�squeda y su bot�n
		add(btnRestBusqueda);
		add(Box.createHorizontalGlue()); //Crear componente vac�o expandible
		add(Box.createRigidArea(new Dimension(4, 0))); //Crear componente vac�o no expandible
		
		for(int i = 0; i < 3; ++i)
		{
			add(new BtnAcciones(i, tabla)); //Crear botones de acci�n
			add(Box.createRigidArea(new Dimension(4, 0))); //A�adir espaciado entre los botones
		}
	}
}

@SuppressWarnings("serial")
class BtnAcciones extends JButton //Clase para los botones del panel
{
	BtnAcciones(JButton btnRestablecer, TablaDatos tabla, JTextField campo) //Constructor para uso con campo de b�squeda
	{
		super(btnRestablecer != null? Imagenes.imgBuscar : Imagenes.imgRestablecer); //A�adir imagen
		setEnabled(btnRestablecer != null); //Desactivar
		setPreferredSize(new Dimension(26,26)); //Ajustar tama�o
		setMaximumSize(new Dimension(26,26));
		ActionListener listener;
		if(btnRestablecer != null) //Si el bot�n no es para restablecer la b�squeda
			listener = new ActionListener() //A�adir listener
			{
				@Override public void actionPerformed(ActionEvent e)
				{
					//Si el campo de b�squeda no est� en blanco, realizar b�squeda
					if(!campo.getText().isBlank())
					{
						btnRestablecer.setEnabled(true);
						tabla.buscarEnTabla(campo.getText());
						btnRestablecer.setEnabled(true);
					}
					else tabla.restablecerTabla(); //De otra manera, restablecer registros
				}					
			};
		else //Si el bot�n es para restablecer la busqueda
			listener = new ActionListener() //A�adir listener
			{
				@Override public void actionPerformed(ActionEvent e)
				{
					//Si el campo de b�squeda no est� en blanco, restablecer registros mostrados
					if(!campo.getText().isBlank()) tabla.restablecerTabla();
					campo.setText(""); //Limpiar campo
					setEnabled(false); //Desactivar bot�n
				}					
			};
		addActionListener(listener); //A�adir listener al bot�n
	}
	
	private final static ImageIcon[] img = {Imagenes.imgRegistrar, Imagenes.imgModificar, Imagenes.imgEliminar},
			img2 = {Imagenes.imgRegistrar2, Imagenes.imgModificar2, Imagenes.imgEliminar2};
	
	BtnAcciones(int id, TablaDatos tabla) //Constructor para botones de acci�n
	{
		super(img[id]); //Crear bot�n con texto dependiendo del id
		setRolloverIcon(img2[id]);
		ActionListener alPresionar; //Listener personalizado
		setBorder(null);
		setContentAreaFilled(false);
		switch(id)
		{
			case 0:
				alPresionar = new ActionListener() //Agregar
				{
					@Override public void actionPerformed(ActionEvent e)
					{
						VentEmergentes.agregarProcesador(false, tabla); //Crear un nuevo registro para la tabla
					}					
				};
				break;
			case 1:
				alPresionar = new ActionListener() //Modificar
				{
					@Override public void actionPerformed(ActionEvent e)
					{
						//Modificar registro solo si hay una sola fila seleccionada
						if(tabla.getSelectedRowCount() != 1)
							VentEmergentes.mostrarError("Aseg�rese de seleccionar una sola fila");
						else VentEmergentes.agregarProcesador(true, tabla);
					}					
				};
				break;
			default: //Si el bot�n dice "Eliminar"
				alPresionar = new ActionListener()
				{
					@Override public void actionPerformed(ActionEvent e)
					{
						//Si no hay filas seleccionadas, mostrar mensaje de error
						if(tabla.getSelectedRowCount() == 0) VentEmergentes.mostrarError("Aseg�rese de seleccionar una o m�s filas");
						else tabla.removerSeleccion(); //De otra manera, remover registros en tabla seleccionados
					}					
				};
		}
		addActionListener(alPresionar); //A�adir listener personalizado
	}
}