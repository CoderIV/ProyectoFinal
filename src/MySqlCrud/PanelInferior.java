package MySqlCrud;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

//Subclase de JPanel para el panel inferior de la ventana

@SuppressWarnings("serial")
public class PanelInferior extends JPanel
{
	public PanelInferior(TablaDatos tabla, ConnectDB conexion) //Constructor
	{
		super(); //Llamada a superconstructor
		setLayout(new BoxLayout(this, BoxLayout.X_AXIS)); //Asignar BoxLayout horizontal como layout
		//setBorder(new EmptyBorder(4,4,4,4)); //A�adir espaciado alrededor
		
		JButton refrescar = new JButton(Imagenes.imgRefrescar); //A�adir bot�n para recargar registros de la base de datos
		refrescar.setPreferredSize(new Dimension(22,22));
		refrescar.setMaximumSize(new Dimension(22,22)); //Modificar tama�o del bot�n
		refrescar.addActionListener(new ActionListener() //A�adir listener con la acci�n deseada
		{
			@Override public void actionPerformed(ActionEvent e)
			{
				tabla.restablecerTabla();
			}
		});
		
		JButton reporte = new JButton(Imagenes.imgReporte); //A�adir bot�n para recargar registros de la base de datos
		reporte.setPreferredSize(new Dimension(22,22));
		reporte.setMaximumSize(new Dimension(22,22)); //Modificar tama�o del bot�n
		reporte.setToolTipText("Vista previa de reporte");
		
		JFrame ventanaPadre = (JFrame)this.getTopLevelAncestor();
		reporte.addActionListener(new ActionListener() //A�adir listener con la acci�n deseada
		{
			@Override public void actionPerformed(ActionEvent e)
			{
				new Reporte(ventanaPadre, conexion.getConnection());
			}
		});
		
		add(refrescar); //A�adir botones al panel
		add(reporte);
		add(Box.createHorizontalGlue()); //Crear componente vac�o que se expanda
		add(new JLabel("Carlos Serrano, Iv�n Vald�s")); //A�adir etiqueta con nombre de los autores
	}
}