package MySqlCrud;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;

//Subclase de JFrame que sirve como ventana para el CRUD

@SuppressWarnings("serial")
public class CRUD extends JFrame
{
	CRUD(ConnectDB conexionDB)
	{
		super("Base de datos - examends3"); //Llamada a superconstructor con t�tulo de la ventana
		TablaDatos tabla = new TablaDatos(conexionDB.getConnection());
		add(new PanelAcciones(tabla), BorderLayout.NORTH); //Agregar componentes a la ventana
		add(new JScrollPane(tabla), BorderLayout.CENTER); 
		add(new PanelInferior(tabla, conexionDB), BorderLayout.SOUTH);
		
		setSize(710, 384); //Asignar tama�o de la ventana
		setLocationRelativeTo(null); //Centra la ventana al ejecutarla
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); //Operaci�n de cerrado
		addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent e) //Cuando se cierre la ventana
            {
            	conexionDB.closeConnection();
                e.getWindow().dispose(); //Destruir ventana
                System.exit(0);
            }
        });
		setVisible(true); //Hacer ventana visible
	}
}
