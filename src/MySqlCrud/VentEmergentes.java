package MySqlCrud;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

import javax.swing.*;
import javax.swing.text.*;

//Subclase de JOptionPane para manejo de ventanas emergentes en el CRUD

@SuppressWarnings("serial")
public class VentEmergentes extends JOptionPane
{
	
	public static void mostrarError(String mensaje) //M�todo para mostrar mensajes de error
	{
		showMessageDialog(null, mensaje, "Error", ERROR_MESSAGE);
	}
	
	//M�todo para que el usuario agregue un registro a la base de datos
	public static void agregarProcesador(boolean modificar, TablaDatos tabla) //M�todo para ingresar una c�dula
	{
		JTextField[] campos = new JTextField[6];
		NumberFormat numFormat = NumberFormat.getIntegerInstance(); //Crear formato para solo n�meros
		numFormat.setGroupingUsed(false); //Desactivar coma o punto
		campos[2] = new JFormattedTextField(new DecimalFormat());
		campos[3] = new JFormattedTextField(numFormat); //Crear campo con formato para solo n�meros
		campos[4] = new JFormattedTextField(new DecimalFormat()); //Crear campo con formato para decimales
		try {campos[5] = new JFormattedTextField(new MaskFormatter("####-##-##"));}
		catch (ParseException e1) {e1.printStackTrace();}
		
		JComponent[] componentes = new JComponent[12]; //Crear arreglo de componentes
		String[] desc = {"Fabricante:", "Modelo:", "Frecuencia (GHz):", "N�cleos:", "Precio:", "Fecha de salida (YYYY-MM-DD):"};
		for(int i = 0; i < 6; i++) //Armar arreglo de componentes
		{
			componentes[i*2] = new JLabel(desc[i]);
			if(i < 2) campos[i] = new campoLimitado(45);
			componentes[i*2+1] = campos[i];
		}
		
		int fila = 0; //Si estamos modificando, establecer fila y a�adir sus valores a los campos
		if(modificar)
		{
			campos[1].setEnabled(false); //Desactivar campo de modelo
			campos[1].setDisabledTextColor(Color.darkGray); //Cambiar color de texto desactivado
			fila = tabla.getSelectedRow();
			//Obtener valores del registro
			for(int i = 0; i < 6; ++i) campos[i].setText(String.valueOf(tabla.getValueAt(fila, i)));
		}
		
		//Mostrar ventana emergente
		while(showConfirmDialog(Principal.ventana, componentes,
				modificar? "Modificar" : "Agregar", OK_CANCEL_OPTION) == OK_OPTION)
		{
			Procesador procesador = new Procesador();
			procesador.setFabricante(campos[0].getText());
			procesador.setModelo(campos[1].getText()); //Obtener datos de los campos
			procesador.setFechasalida(campos[5].getText() + " 00:00:00");
			try
			{
				procesador.setFrecuencia(Float.parseFloat(campos[2].getText()));
				procesador.setNucleos(Integer.parseInt(campos[3].getText()));
				procesador.setPrecio(Double.parseDouble(campos[4].getText()));
			}
			catch(Exception e) //Si encontramos un error durante parse, quiere decir que los campos estan vac�os
			{
				mostrarError("Los campos Frecuencia, N�cleos y Precio no pueden estar vac�os");
				continue;
			}
			
			if(modificar) //Si estamos modificando, llamar al m�todo correspondiente
			{
				if(tabla.model.Modificar(procesador))
				{
					tabla.modificarEnTabla(fila, procesador.obtenerAtrib());
					break;
				}
			}
			else if(tabla.model.Crear(procesador)) //De otra manera, se crea el registro y se a�ade como nueva fila
			{
				tabla.a�adirEnTabla(procesador.obtenerAtrib());
				break;
			}
		}
	}
}

//Clase derivada de JTextField que limita la cantidad de caracteres que pueden ser ingresados

@SuppressWarnings("serial")
class campoLimitado extends JTextField
{
	campoLimitado(int lim) //Constructor con argumento entero que define el l�mite
	{
		super(); //Llamada a superconstructor
		setDocument(new PlainDocument() //Limitar caracteres
		{
			public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
			{
				if(str == null || offs >= lim) return; //Si ya se excedi� el l�mite, descartar
				//De otra manera, solo tomar lo suficiente para no llegar al m�mite
				else if(offs + str.length() > lim) str = str.substring(0, lim - offs);
				super.insertString(offs, str, a);
			}
		});
	}
}