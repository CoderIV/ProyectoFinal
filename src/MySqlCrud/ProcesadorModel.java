package MySqlCrud;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProcesadorModel
{
	private Connection conexion;
	
	public ProcesadorModel(Connection conexion)
	{
		this.conexion = conexion;
		verificarExistenciaTabla();
	}
	
	public boolean Crear(Procesador p) //M�todo para crear un registro
	{
		try
		{
			//Crear comando SQL
			PreparedStatement ps = conexion
					.prepareStatement("insert into procesadores values(?, ?, ?, ?, ?, ?)");
			ps.setString(1, p.getFabricante());
			ps.setString(2, p.getModelo()); //Establecer argumentos
			ps.setFloat(3, p.getFrecuencia());
			ps.setInt(4, p.getNucleos());
			ps.setDouble(5, p.getPrecio());
			ps.setString(6, p.getFechasalida());
			return ps.executeUpdate() > 0; //Devolver verdadero solo si el comando devolvi� algo
		}
		catch (Exception e)
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage()); //En caso de error, mostrar mensaje
			return false;
		}

	}

	public Procesador buscar(String modelo) //M�todo para buscar un registro por modelo
	{
		try
		{
			Procesador p = null;
			//Crear comando SQL
			PreparedStatement ps = conexion.prepareStatement("select * from procesadores where modelo=?");
			ps.setString(1, modelo); //Establecer argumento
			ResultSet rs = ps.executeQuery(); //Ejecutar comando
			while (rs.next())
			{
				p = new Procesador(); //Si encontramos el registro, armar objeto Procesador con datos obtenidos
				p.setFabricante(rs.getString("fabricante"));
				p.setModelo(rs.getString("modelo"));
				p.setFrecuencia(rs.getFloat("frecuencia"));
				p.setNucleos(rs.getInt("nucleos"));
				p.setPrecio(rs.getDouble("precio"));
				p.setFechasalida(rs.getString("fechasalida"));
			}
			return p; //Devolver objeto Procesador
		}
		catch (Exception e)
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage()); //En caso de error, mostrar mensaje
			return null;
		}
	}
	
	public List<Procesador> buscarTodo() //M�todo para obtener todos los registros
	{
		try
		{	
			List<Procesador> listaProcesador = new ArrayList<>(); //Inicializar lista
			//Crear comando SQL
			PreparedStatement ps = conexion.prepareStatement("select * from procesadores");
			//Ejecutar comando
			ResultSet rs = ps.executeQuery();
			while (rs.next()) //Mientras encontremos registros, a�adirlos a la lista
			{
				Procesador p = new Procesador();
				p.setFabricante(rs.getString("fabricante"));
				p.setModelo(rs.getString("modelo"));
				p.setFrecuencia(rs.getFloat("frecuencia"));
				p.setNucleos(rs.getInt("nucleos"));
				p.setPrecio(rs.getDouble("precio"));
				p.setFechasalida(rs.getString("fechasalida"));
				listaProcesador.add(p);
			}
			return listaProcesador; //Retornar lista de registros
		}
		catch (Exception e)
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage()); //En caso de error, mostrar mensaje
			return null;
		}
	}

	public boolean Modificar(Procesador p) //M�todo para modificar un registro
	{
		try
		{
			//Crear comando SQL
			PreparedStatement ps = conexion.prepareStatement(
					"update procesadores set fabricante=?, frecuencia=?, nucleos=?, precio=?, fechasalida=? where modelo=?");
			ps.setString(1, p.getFabricante());
			ps.setFloat(2, p.getFrecuencia());
			ps.setInt(3, p.getNucleos());
			ps.setDouble(4, p.getPrecio());
			ps.setString(5, p.getFechasalida());
			ps.setString(6, p.getModelo());
			return ps.executeUpdate() > 0; //Devolver verdadero solo si el comando devolvi� algo
		}
		catch (Exception e)
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage()); //En caso de error, mostrar mensaje
			return false;
		}
	}

	public Boolean Eliminar(String modelo) //M�todo para eliminar registros por modelo
	{
		try
		{
			//Crear comando SQL
			PreparedStatement ps = conexion.prepareStatement("delete from procesadores where modelo=?");
			ps.setString(1, modelo); //Establecer argumento
			return ps.executeUpdate() > 0; //Devolver verdadero solo si el comando devolvi� algo
		}
		catch (Exception e)
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage()); //En caso de error, mostrar mensaje
			return null;
		}
	}
	
	private void verificarExistenciaTabla() //M�todo para verificar la existencia de la tabla
	{
		try
		{
			DatabaseMetaData md = conexion.getMetaData();
			ResultSet table = md.getTables(null, null, "procesadores", null); //Obtener tablas de la DB
			if(!table.next()) //Si no encontramos la tabla
			{
				//Crear la tabla
				conexion.createStatement().execute("CREATE TABLE procesadores("
	                    + "`fabricante` VARCHAR(45) NOT NULL,"
	                    + "`modelo` VARCHAR(45) NOT NULL,"
	                    + "`frecuencia` float NOT NULL,"
	                    + "`nucleos` INT NOT NULL,"
	                    + "`precio` Double NOT NULL,"
	                    + "`fechasalida` DATETIME NOT NULL,"
	                    + "PRIMARY KEY (`modelo`));");
				Object[][] datosDefault = {
						{"AMD", "Ryzen 5 5600X", 4.6f, 6, 174.99, "2020-11-05 00:00:00"},
						{"AMD", "Ryzen 9 5900X", 4.8f, 12, 389.99, "2020-10-08 00:00:00"},
						{"Intel", "Core i9-11900K ", 5.3f, 8, 355.99, "2021-03-15 00:00:00"},
						{"AMD", "Ryzen 7 5800X", 4.7f, 8, 274.99, "2020-11-05 00:00:00"},
						{"Intel", "Core i9-12900K", 5.2f, 16, 559.99, "2022-04-05 00:00:00"},
						{"Intel", "Core i9-10850K", 5.2f, 10, 337.95, "2020-07-28 00:00:00"},
						{"Intel", "Core i7-12700KF", 5.0f, 12, 377.98, "2021-11-04 00:00:00"},
						{"AMD", "Ryzen 7 3800X", 4.5f, 8, 287.99, "2020-06-07 00:00:00"},
						{"AMD", "Ryzen 5 5600G", 4.4f, 6, 178.99, "2021-04-13 00:00:00"},
						{"Intel", "Core i7-11700K", 3.6f, 8, 309.00, "2021-03-16 00:00:00"}};
				
				//Insertar datos por defecto
				Procesador procesador = new Procesador();
				for(Object[] datos : datosDefault)
				{
					procesador.setFabricante((String)datos[0]);
					procesador.setModelo((String)datos[1]);
					procesador.setFrecuencia((float)datos[2]);
					procesador.setNucleos((int)datos[3]);
					procesador.setPrecio((double)datos[4]);
					procesador.setFechasalida((String)datos[5]);
					Crear(procesador);
				}
			}
		}
		catch (SQLException e) //Si encontramos un error, mostr�rlo en una ventana emergente
		{
			VentEmergentes.mostrarError(e.getLocalizedMessage());
		}
	}
}
